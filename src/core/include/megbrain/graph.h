/**
 * \file src/core/include/megbrain/graph.h
 * MegEngine is Licensed under the Apache License, Version 2.0 (the "License")
 *
 * Copyright (c) 2014-2021 Megvii Inc. All rights reserved.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT ARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

#pragma once
#define MGB_CAMBRICON 1
#define MGB_ATLAS 1
#define MGB_ENABLE_TENSORT_RT 1
#define MGB_ENABLE_COREML 1
#include "megbrain/graph/cg.h"
#include "megbrain/graph/helper.h"

namespace mgb {

using cg::VarNode;
using cg::VarNodeArray;
using cg::GraphError;
using cg::ComputingGraph;
using cg::SymbolVar;
using cg::SymbolVarArray;
using cg::VarNodeArrayView;
using cg::SymbolVarArrayView;
using cg::OperatorNodeConfig;

} // namespace mgb

// vim: syntax=cpp.doxygen foldmethod=marker foldmarker=f{{{,f}}}

