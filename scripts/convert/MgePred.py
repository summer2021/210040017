# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# Download an example image from the megengine data website
# import urllib.request
# url, filename = ("https://data.megengine.org.cn/images/cat.jpg", "cat.jpg")
# try:
#     urllib.request.URLopener().retrieve(url, filename)
# except:
#     urllib.request.urlretrieve(url, filename)

# Read and pre-process the image
import cv2
import numpy as np
import coremltools as ct


import matplotlib.pyplot as plt
import megengine.data.transform as T
import megengine.functional as F
from megengine import jit, tensor

import megengine.module.external

import megengine.hub
# model = megengine.hub.load('megengine/models', 'resnet18', pretrained=True)
# or any of these variants
# model = megengine.hub.load('megengine/models', 'resnet34', pretrained=True)
model = megengine.hub.load('megengine/models', 'resnet50', pretrained=True)
# model = megengine.hub.load('megengine/models', 'resnet101', pretrained=True)
# model = megengine.hub.load('megengine/models', 'resnet152', pretrained=True)
# model = megengine.hub.load('megengine/models', 'resnext50_32x4d', pretrained=True)
model.eval()



image = cv2.imread("lion.png")
transform = T.Compose([
    T.Resize(256),
    T.CenterCrop(224),
    T.Normalize(mean=[103.530, 116.280, 123.675], std=[57.375, 57.120, 58.395]),  # BGR
    T.ToMode("CHW"),
])
processed_img = transform.apply(image)[np.newaxis, :]  # CHW -> 1CHW
logits = model(processed_img)
probs = F.softmax(logits)
print(probs)

model_cl = ct.models.MLModel('./resnet50.mlmodel')
out_dict = model_cl.predict({'data': processed_img})

print((np.argmax(np.array(probs)), np.argmax(out_dict['TRUE_DIV(EXP_Reduce)[9473]'])))
a=1
pass


@jit.trace(symbolic=True, capture_as_const=True)
def fun(data, *, net):
    pred = net(data)
    pred_normalized = F.softmax(pred)
    return pred_normalized


data = tensor(np.random.random([1, 3, 224, 224]).astype(np.float32))

fun(data, net=model)
fun.dump("resnet50.mge", arg_names=["data"])

