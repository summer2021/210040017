import coremltools as ct
import numpy as np
import PIL.Image
import matplotlib.pyplot as plt

# load a model whose input type is "Image"
model = ct.models.MLModel('./resnet502.mlmodel')

Height = 256  # use the correct input image height
Width = 256  # use the correct input image width





# Scenario 1: load an image from disk
def load_image(path, resize_to=None):
    # resize_to: (Width, Height)
    img = PIL.Image.open(path)
    if resize_to is not None:
        img = img.resize(resize_to, PIL.Image.ANTIALIAS)
        img = img.crop((16,16,240,240))
    img_np = np.array(img).astype(np.float32)
    # img_np2 = img_np[:,:,::-1]
    # img_mean = np.mean(img_np, axis=(0,1))
    # img_std = np.std(img_np, axis=(0,1))
    mean = [123.675, 116.280, 103.530]
    std = [58.395, 57.120, 57.375]
    for i in range(3):
        img_np[:, :, i] = (img_np[:, :, i]-mean[i])/std[i]

    img_np2 = np.zeros((1,3,224,224))
    # img_np2[0,0:2,:,:] = img_np[:, :, ::-1]
    # img_np2 = img_np2[np.newaxis, :]
    # np.normalize(mean=[103.530, 116.280, 123.675], std=[57.375, 57.120, 58.395])
    return img_np2, img


# load the image and resize using PIL utilities
img_np2, img = load_image('./lion.png', resize_to=(Width, Height))
out_dict = model.predict({'data': img})

# Scenario 2: load an image from a numpy array
shape = (Height, Width, 3)  # height x width x RGB
data = np.zeros(shape, dtype=np.uint8)
# manipulate numpy data
pil_img = PIL.Image.fromarray(data)
out_dict = model.predict({'data': pil_img})

pass