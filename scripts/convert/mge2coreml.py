import coremltools as ct
import onnx
import .onnx_converter as moc
import coremltools.proto.FeatureTypes_pb2 as ft
import copy

 md_mge = './resnet50.mge'
 moc.onnx_converter.convert_to_onnx(md_mge)

 md = onnx.load_model('./out.onnx')
# G = md.graph
# N = G.node
# Convert from ONNX to Core ML

label = './ImageNet_label'
inputs = [ct.ImageType(color_layout="BGR")]
preprocessing_args = {'is_bgr': True, 'red_bias': -123.675 / 58.395, 'green_bias': -116.280 / 57.120,
                      'blue_bias': -103.530 / 57.375}
# mean=[103.530, 116.280, 123.675], std=[57.375, 57.120, 58.395])
model = ct.converters.onnx.convert(model='./out.onnx', minimum_ios_deployment_target='13', mode='classifier',
                                   class_labels=label, preprocessing_args=preprocessing_args)
model.save('resnet502.mlmodel')

spec = model.get_spec()
input = spec.description.input[0]
input.type.imageType.colorSpace = ft.ImageFeatureType.BGR
input.type.imageType.height = 224
input.type.imageType.width = 224

nn_spec = spec.neuralNetworkClassifier
layers = nn_spec.layers  # this is a list of all the layers
layers_copy = copy.deepcopy(layers)  # make a copy of the layers, these will be added back later
del nn_spec.layers[:]  # delete all the layers
## add a scale layer now
# since mlmodel is in protobuf format, we can add proto messages directly
# To look at more examples on how to add other layers: see "builder.py" file in coremltools repo
scale_layer = nn_spec.layers.add()
scale_layer.name = 'scale_layer'
scale_layer.input.append('data')
scale_layer.output.append('data_scaled')
params = scale_layer.scale
params.scale.floatValue.extend([1 / 57.375, 1 / 57.120, 1 / 58.395])  # scale values for BGR
params.shapeScale.extend([3, 1, 1])  # shape of the scale vector

# now add back the rest of the layers (which happens to be just one in this case: the crop layer)
nn_spec.layers.extend(layers_copy)

# need to also change the input of the crop layer to match the output of the scale layer
nn_spec.layers[1].input[0] = 'data_scaled'

ct.utils.save_spec(spec, "resnet502.mlmodel")
pass
