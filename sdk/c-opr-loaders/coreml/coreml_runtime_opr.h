//
//  coreml_runtime_opr.h
//  meg_coreML
//
//  Created by macOS on 2021/8/12.
//  Copyright © 2021 macOS. All rights reserved.
//
#pragma once

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wdocumentation"
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#include "megbrain/comp_node_env.h"
#include "megbrain/graph.h"
#include "megbrain/serialization/file.h"
#pragma clang diagnostic pop



#import <Foundation/Foundation.h>
#import <CoreML/CoreML.h>

#if MGB_ENABLE_COREML
 


namespace mge {
namespace opr{
MGB_DEFINE_OPR_CLASS(CoreMLRuntimeOpr, cg::SingleCNOutshapePureByInshapeOprBase) // {
{
public:
    using SharedBuffer = mgb::serialization::SharedBuffer;

    void scn_do_execute() override;
    void get_output_var_shape(const TensorShapeArray& inp_shape,
                              TensorShapeArray& out_shape) const override;
    void add_input_layout_constraint() override;
    void init_output_dtype() override;
    
    CoreMLRuntimeOpr(SharedBuffer buf, const VarNodeArray& inputs, const OperatorNodeConfig& config);

    
    static SymbolVarArray make(SharedBuffer buf, const SymbolVarArray& src,
                               const OperatorNodeConfig& config = {});
    
    static SymbolVarArray make(const void* buf, size_t size,
                               const SymbolVarArray& src,
                               const OperatorNodeConfig& config = {});
    
private:
    // TODO: add unique ptr check for m_model
    MLModel *m_model = nil;
    NSURL *compiledUrl = nil;
    
    // CnrtModelUniquePtr m_model;
    // CnrtFunctionUniquePtr m_function;
    // CnrtRuntimeContextUniquePtr m_context;
    // bool m_tensor_dim_mutable;
};

}
}

#endif  //MGB_ENABLE_COREML
