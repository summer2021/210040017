#include <stdlib.h>
#include <iostream>
#include "megbrain/serialization/serializer.h"
using namespace mgb;
using namespace std;

cg::ComputingGraph::OutputSpecItem make_callback_copy(SymbolVar dev,
                                                      HostTensorND& host) {
    auto cb = [&host](DeviceTensorND& d) { host.copy_from(d); };
    return {dev, cb};
}

int main(int argc, char* argv[]) {
    std::cout << " Usage: ./coreml_deploy model_name data_input data_output"
              << std::endl;
    if (argc != 4) {
        std::cout << " Wrong argument" << std::endl;
        return 0;
    }
    std::unique_ptr<serialization::InputFile> inp_file =
            serialization::InputFile::make_fs(argv[1]);
    fstream fin1;
    fstream fin2;
    fin1.open(argv[2], ios::in);
    fin2.open(argv[3], ios::in);

    auto loader = serialization::GraphLoader::make(std::move(inp_file));
    serialization::GraphLoadConfig config;
    serialization::GraphLoader::LoadResult network =
            loader->load(config, false);
    auto data = network.tensor_map["data"];
    float* data_ptr = data->resize({3*224*224}).ptr<float>();
    float res_ptr[1000];

//     data_ptr[0] = x;
//     data_ptr[1] = y;vector<string> row;
    string line, word, temp;
    int i = 0;
    while (fin1 >> temp) {
        row.clear();
        getline(fin1, line);
        stringstream s(line);
        while (getline(s, word, ', ')) {
            row.push_back(word);
        }
        data_ptr[i++] = stoi(row[0]);
        if (i==3*224*224){
            break;
        }
    }

    i = 0;
    while (fin2 >> temp) {
        row.clear();
        getline(fin2, line);
        stringstream s(line);
        while (getline(s, word, ', ')) {
            row.push_back(word);
        }
        res_ptr[i++] = stoi(row[0]);
        if (i==1000){
            break;
        }
    }

    HostTensorND predict;
    std::unique_ptr<cg::AsyncExecutable> func =
            network.graph->compile({make_callback_copy(
                    network.output_var_map.begin()->second, predict)});
    func->execute();
    func->wait();
    float* predict_ptr = predict.ptr<float>();
    float max_err = -1e20;
    int max_err_idx = -1;
    for (i=0; i<1000; ++i){
        max_err = abs(predict_ptr[i]-res_ptr[i])>max?abs(predict_ptr[i]-res_ptr[i]):max;
        max_err_idx = abs(predict_ptr[i]-res_ptr[i])>max?i:max_err_idx;
    }
    std::cout << " Predicted max err: " << max_err << " @"<<max_err_idx;

}
