//
//  coreml_runtime.cpp
//  meg_coreML
//
//  Created by macOS on 2021/8/11.
//  Copyright © 2021 macOS. All rights reserved.
//

#include "../op_trait.h"
#include "megbrain/imperative/ops/autogen.h"

#if MGB_ENABLE_COREML
#include "megbrain/coreml/coreml_runtime_opr.h"
namespace mgb::imperative {

namespace { namespace coreml_runtime {
    auto apply_on_var_node(
            const OpDef& def,
            const VarNodeArray& inputs) {
        auto&& op = static_cast<const CoreMLRuntime&>(def);
        OperatorNodeConfig config{op.make_name()};
        SymbolVarArray sinputs(inputs.begin(), inputs.end());
        return opr::CoreMLRuntimeOpr::make(op.buf.c_str(), op.buf_size, sinputs, config);
    }
OP_TRAIT_REG(CoreMLRuntime, CoreMLRuntime)
    .apply_on_var_node(apply_on_var_node)
    .fallback();
}} // coreml_runtime

} // namespace mgb::imperative
#endif  // MGB_ENABLE_COREML
